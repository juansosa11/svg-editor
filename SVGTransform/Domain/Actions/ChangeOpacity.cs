using System.Linq;
using System.Xml.Linq;
using SVGTransform.Domain.Actions.Interfaces;

namespace SVGTransform.Domain.Actions
{
    public class ChangeOpacity : IChangeOpacity
    {
        public void Execute(string opacity, XDocument svg)
        {
            if (string.IsNullOrEmpty(opacity)) return;

            opacity = opacity.Replace(',', '.');
            var pathNodes = svg.Descendants().Where(p => p.Name.LocalName == "path").ToList();

            foreach (var node in pathNodes)
            {
                const string xName = "style";
                if (node.Attribute(xName) != null)
                {
                    var wasModified = true;
                    var newStyles = string.Empty;
                    foreach (var entries in node.Attribute(xName).Value.Split(';'))
                    {
                        if (string.IsNullOrEmpty(entries)) continue;
                        var values = entries.Split(':');
                        if (values[0].ToLower().Trim() == "opacity" || 
                            values[0].ToLower().Trim() == "fill-opacity")
                        {
                            values[1] = opacity.ToString();
                            newStyles += $"{values[0]}: {values[1]};";
                            wasModified = false;
                        }
                        else
                        {
                            newStyles += entries + ";";
                        }
                    }
                    
                    if (wasModified)
                    {
                        newStyles += $"opacity: {opacity};";
                    }
                    
                    node.Attribute(xName).Value = newStyles;
                }
                else
                {
                    node.SetAttributeValue(xName, $"opacity: {opacity};");
                }
            }
        }
    }
}