using System.Linq;
using System.Xml.Linq;
using SVGTransform.Domain.Actions.Interfaces;

namespace SVGTransform.Domain.Actions
{
    public class ScaleAllSvg : IScaleAllSvg
    {
        public void Execute(int? scale, XDocument svg)
        {
            if (scale == null || scale == 0) return;
            
            var svgNode = svg.Descendants().FirstOrDefault(node => node.Name.LocalName == "svg");
            if (svgNode == null) return;

            if (svgNode.Attribute("width") != null && svgNode.Attribute("height") != null)
            {
                svgNode.Attribute("width").Value = (int.Parse(svgNode.Attribute("width").Value.Replace("px", "")) + scale).ToString();
                svgNode.Attribute("height").Value = (int.Parse(svgNode.Attribute("height").Value.Replace("px", "")) + scale).ToString();
            }
            else
            {
                var viewBoxValues = svgNode.Attribute("viewBox").Value.Split(' ');
                var newWidth = int.Parse(viewBoxValues[2]) + scale;
                var newHeight = int.Parse(viewBoxValues[3]) + scale;
                svgNode.Add(new XAttribute("width", $"{newWidth}px"));
                svgNode.Add(new XAttribute("height", $"{newHeight}px"));
            }
        }
    }
}