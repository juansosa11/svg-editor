using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml.Linq;
using Svg;
using SVGTransform.Domain.Actions.Interfaces;

namespace SVGTransform.Domain.Actions
{
    public class ConvertToBitmap : IConvertToBitmap
    {
        public string Execute(string color, XDocument xDocument)
        {
            var name = Guid.NewGuid().ToString("N");
            xDocument.Save($"{name}.svg");
            var svgDocument = SvgDocument.Open($"{name}.svg");
            string base64;
            using (var bitmap = svgDocument.Draw())
            {
                using (var finalBitmap = new Bitmap(bitmap.Width, bitmap.Height))
                {
                    using (var g = Graphics.FromImage(finalBitmap))
                    {
                        BitmapChangeColor(color, g, bitmap);
                        g.DrawImage(bitmap, 0, 0, bitmap.Width, bitmap.Height);
                    }
                    base64 = BitmapToBase64(finalBitmap);
                }
            }
            
            File.Delete($"{name}.svg");
            return base64;
        }

        private static void BitmapChangeColor(string color, Graphics g, Bitmap bitmap)
        {
            if (string.IsNullOrEmpty(color)) return;
            
            var colorBackground = ColorTranslator.FromHtml("#" + color);
            using (var brush = new SolidBrush(colorBackground))
            {
                g.FillRectangle(brush, 0, 0, bitmap.Width, bitmap.Height);
            }
        }

        private string BitmapToBase64(Image image)
        {
            var ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            byte[] byteImage = ms.ToArray();
            return Convert.ToBase64String(byteImage);
        }
    }
}