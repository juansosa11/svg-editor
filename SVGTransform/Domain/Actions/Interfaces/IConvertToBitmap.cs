using System.Xml.Linq;

namespace SVGTransform.Domain.Actions.Interfaces
{
    public interface IConvertToBitmap
    {
        string Execute(string color, XDocument xDocument);
    }
}