using System.Xml.Linq;

namespace SVGTransform.Domain.Actions.Interfaces
{
    public interface IRotate
    {
        void Execute(int? rotateDegree, XDocument svg);
    }
}