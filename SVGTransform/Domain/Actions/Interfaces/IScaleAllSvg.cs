using System.Xml.Linq;

namespace SVGTransform.Domain.Actions.Interfaces
{
    public interface IScaleAllSvg
    {
        void Execute(int? scale, XDocument svg);
    }
}