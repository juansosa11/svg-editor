using System.Xml.Linq;

namespace SVGTransform.Domain.Actions.Interfaces
{
    public interface IChangeOpacity
    {
        void Execute(string opacity, XDocument svg);
    }
}