using System.Linq;
using System.Xml.Linq;
using SVGTransform.Domain.Actions.Interfaces;

namespace SVGTransform.Domain.Actions
{
    public class Rotate : IRotate
    {
        public void Execute(int? rotateDegree, XDocument svg)
        {
            if (rotateDegree == null || rotateDegree == 0) return;
            var svgNode = svg.Descendants().FirstOrDefault(node => node.Name.LocalName == "svg");
            if (svgNode == null) return;
            
            var xPoint = 0;
            var yPoint = 0;
            var width = 0;
            var height = 0;
            
            if (svgNode.Attribute("width") != null && svgNode.Attribute("height") != null)
            {
                xPoint = svgNode.Attribute("x") != null ? int.Parse(svgNode.Attribute("x").Value) : 0;
                yPoint = svgNode.Attribute("y") != null ? int.Parse(svgNode.Attribute("y").Value) : 0;
                width = svgNode.Attribute("width") != null ? int.Parse(svgNode.Attribute("width").Value.Replace("px", "")) : 0;
                height = svgNode.Attribute("height") != null ? int.Parse(svgNode.Attribute("height").Value.Replace("px", "")) : 0;
            }
            else
            {
                var viewBoxValues = svgNode.Attribute("viewBox").Value.Split(' ');
                xPoint = int.Parse(viewBoxValues[0]);
                yPoint = int.Parse(viewBoxValues[1]);
                width = int.Parse(viewBoxValues[2]);
                height = int.Parse(viewBoxValues[3]);
            }

            var x = xPoint + width / 2;
            var y = yPoint + height / 2;
            var svgDescendants = svgNode.Descendants();
            if (svgDescendants.Any(node => node.Name.LocalName == "g"))
            {
                var gNode = svgDescendants.FirstOrDefault(node => node.Name.LocalName == "g");
                if (gNode.Attribute("transform") == null)
                {
                    gNode.Add(new XAttribute("transform", $"rotate({rotateDegree} {x} {y})"));
                }
                else
                {
                    gNode.Attribute("transform").Value += $" rotate({rotateDegree} {x} {y})";
                }
            }
            else
            {
                var gNode = new XElement("g");
                gNode.Add(new XAttribute("transform", $"rotate({rotateDegree} {x} {y})"));
                gNode.Name = svgNode.Name.Namespace + gNode.Name.LocalName;
                gNode.Add(svgDescendants);
                svgNode.ReplaceNodes(gNode);
            }
        }
    }
}