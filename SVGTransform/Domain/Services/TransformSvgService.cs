using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Xml.Linq;
using SVGTransform.Domain.Actions;
using SVGTransform.Domain.Actions.Interfaces;
using SVGTransform.Models;

namespace SVGTransform.Domain.Services
{
    public class TransformSvgService : ITransformSvgService
    {
        private readonly IChangeOpacity _changeOpacity;
        private readonly IScaleAllSvg _scaleAllSvg;
        private readonly IRotate _rotate;
        private readonly IConvertToBitmap _convertToBitmap;

        public TransformSvgService(IChangeOpacity changeOpacity, IScaleAllSvg scaleAllSvg, IRotate rotate, IConvertToBitmap convertToBitmap)
        {
            _rotate = rotate;
            _changeOpacity = changeOpacity;
            _scaleAllSvg = scaleAllSvg;
            _convertToBitmap = convertToBitmap;
        }
        
        public ContentImage Execute(ContentSvg contentSvg, HttpPostedFileBase file)
        {
            var svg = XDocument.Load(new StreamReader(file.InputStream));
            
            var image = new ContentImage();
            
            image.OriginalImage = _convertToBitmap.Execute(string.Empty, svg);
            _changeOpacity.Execute(contentSvg.Opacity, svg);
            _rotate.Execute(contentSvg.AngleDegree, svg);
            _scaleAllSvg.Execute(contentSvg.Scale, svg);
            image.ModifiedImage = _convertToBitmap.Execute(contentSvg.ColorBackground, svg);

            return image;
        }
    }
}