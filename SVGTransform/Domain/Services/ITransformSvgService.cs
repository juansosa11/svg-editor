using System.Web;
using SVGTransform.Models;

namespace SVGTransform.Domain.Services
{
    public interface ITransformSvgService
    {
        ContentImage Execute(ContentSvg contentSvg, HttpPostedFileBase file);
    }
}