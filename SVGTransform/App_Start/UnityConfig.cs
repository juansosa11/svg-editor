using System.Web.Mvc;
using SVGTransform.Domain.Actions;
using SVGTransform.Domain.Actions.Interfaces;
using SVGTransform.Domain.Services;
using Unity;
using Unity.Mvc5;

namespace SVGTransform
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            container.RegisterType<IConvertToBitmap, ConvertToBitmap>();
            container.RegisterType<IRotate, Rotate>();
            container.RegisterType<IScaleAllSvg, ScaleAllSvg>();
            container.RegisterType<IChangeOpacity, ChangeOpacity>();
            container.RegisterType<ITransformSvgService, TransformSvgService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}