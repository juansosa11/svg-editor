using System.ComponentModel.DataAnnotations;

namespace SVGTransform.Models
{
    public class ContentSvg
    {
        [Display(Name = "Opacity (0 to 1)")]
        [RegularExpression(@"^(?:1?\d(?:\.\d{1,2})?|1(?:\.0?0?)?)$", ErrorMessage = "Must be a number from 0 to 1")]
        public string Opacity { get; set; }
        
        [Display(Name = "Rotate")]
        public int AngleDegree { get; set; }
        
        [Display(Name = "Color Background")]
        public string ColorBackground { get; set; }

        [Display(Name = "Scale in PX")]
        public int Scale { get; set; }

        public ContentImage Image { get; set; }
    }
}