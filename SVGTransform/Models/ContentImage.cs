namespace SVGTransform.Models
{
    public class ContentImage
    {
        public string ModifiedImage { get; set; }
        public string OriginalImage { get; set; }
    }
}