﻿using System.Web;
using System.Web.Mvc;
using SVGTransform.Domain.Services;
using SVGTransform.Models;

namespace SVGTransform.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITransformSvgService _svgService;
        public HomeController(ITransformSvgService svgService)
        {
            _svgService = svgService;
        }
        
        [HttpGet]
        public ActionResult Index()
        {
            return View("Index");
        }
        
        [HttpPost]
        public ActionResult Index(ContentSvg model)
        {
            HttpPostedFileBase file = Request.Files["ImageData"];

            var contentImage = _svgService.Execute(model, file);
            model.Image = contentImage;
            
            return View("Index", model);
        }
    }
}